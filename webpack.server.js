const path = require('path');
const merg = require('webpack-merge');
const baseConfig = require('./webpack.base');
const webpackNodeExternals = require('webpack-node-externals');

const config = {
    // Know webpack we building a bundle for nodeJs
    target: 'node',


    //tell webpack the root file of our server application
    entry: './src/index.js',

    // tell webpack where to put the output file that is generated
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },
    
    externals: [webpackNodeExternals()]
};

module.exports = merg(baseConfig, config);