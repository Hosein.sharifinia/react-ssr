import HomePage from './pages/HomePage';
import UserListsPage from './pages/UserListsPage';
import App from './App';
import NotFoundPage from './pages/NotFoundPage';
import AdminsListPage from './pages/AdminsListPage';

export default [
    {
        ...App,
        routes : [
            {
                ...HomePage,
                path: '/',
                exact: true
            },
            {
                ...AdminsListPage,
                path: '/admins'
            },
            {
                ...UserListsPage,
                path:'/users',
            },
            {
                ...NotFoundPage
            }
        ]
    }
];